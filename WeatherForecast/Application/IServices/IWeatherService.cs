﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public interface IWeatherService
    {
        public string WeatherLink { get;}
        public string ApiId { get;}
        public WeatherData GetWeather(WeatherData data,int days);
    }
}
