﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class AddCitiDto
    {
        public string CityName { get; set; }
    }
}
