﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class GetWeatherDto
    {
        [Required]
        public string Location { get; set; }

        public int Days { get; set; } = 3;
    }
}
