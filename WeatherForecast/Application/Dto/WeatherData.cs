﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class WeatherData
    {
        public City city { get; set; }
        public List<WeatherInfo> list { get; set; } = new List<WeatherInfo>();
    }
}
