﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class WeatherInfoComparer : IEqualityComparer<WeatherInfo>
    {
        public bool Equals([AllowNull] WeatherInfo x, [AllowNull] WeatherInfo y)
        {
            if (Object.ReferenceEquals(x, y)) return true;

            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            return x.date.Day == y.date.Day;
        }

        public int GetHashCode([DisallowNull] WeatherInfo obj)
        {
            if (Object.ReferenceEquals(obj, null)) return 0;

            int hashProductCode = obj.date.Day.GetHashCode();

            return hashProductCode;
        }
    }
}
