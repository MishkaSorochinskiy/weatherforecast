﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application
{
    public class WeatherInfo
    {
        public string dt_txt { get; set; }

        public DateTime date { 
            get 
            {
                DateTime curdate = new DateTime();
               
                DateTime.TryParse(this.dt_txt , out curdate);
                
                return curdate;
            }
        }

        public Main main { get; set; }

        public List<Weather> weather { get; set; } = new List<Weather>();
        
        public Clouds clouds { get; set; }
       
        public Wind wind { get; set; }
    }
}
