﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class SavedPlaceConfig : IEntityTypeConfiguration<SavedPlace>
    {
        public void Configure(EntityTypeBuilder<SavedPlace> builder)
        {
            builder.ToTable("SavedPlaces");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name).IsRequired().HasMaxLength(30);
        }
    }
}
