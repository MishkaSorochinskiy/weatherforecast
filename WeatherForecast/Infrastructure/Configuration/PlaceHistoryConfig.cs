﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class PlaceHistoryConfig : IEntityTypeConfiguration<PlaceHistory>
    {
        public void Configure(EntityTypeBuilder<PlaceHistory> builder)
        {
            builder.ToTable("PlacesHistroy");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name).IsRequired().HasMaxLength(30);

            builder.Property(p => p.timeViewed).IsRequired();
        }
    }
}
