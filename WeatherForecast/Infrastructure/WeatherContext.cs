﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class WeatherContext:DbContext
    {
        public DbSet<SavedPlace> SavedPlaces { get; set; }

        public DbSet<PlaceHistory> PlacesHistory { get; set; }

        public WeatherContext(DbContextOptions options):base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new SavedPlaceConfig());

            modelBuilder.ApplyConfiguration(new PlaceHistoryConfig());

        }
    }
}
