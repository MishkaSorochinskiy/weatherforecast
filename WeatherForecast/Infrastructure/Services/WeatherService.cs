﻿using Application;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Infrastructure
{
    public class WeatherService : IWeatherService
    {
        public string WeatherLink => $"http://api.openweathermap.org/data/2.5/forecast?";

        public string ApiId => $"58a260f3fff67590e4ee7d2121768c3d";

        public IUnitOfWork UnitOfWork { get; set; }

        public WeatherService(IUnitOfWork unitofwork)
        {
            UnitOfWork = unitofwork;
        }

        public WeatherData GetWeather(WeatherData data,int days)
        {
            var uniquedata = new WeatherData() { list = new List<WeatherInfo>() { data.list[0] }, city = new City() { name = data.city.name, country = data.city.country } };

            data.list.RemoveAll((w) => { return w.date.Day == uniquedata.list[0].date.Day || (w.date.Hour > 19 || w.date.Hour < 10); });

            uniquedata.list.AddRange(data.list.Distinct(new WeatherInfoComparer()));

            uniquedata.list = uniquedata.list.Take(days).ToList();

            foreach(var weather in uniquedata.list)
            {
                weather.main.temp = Math.Round(weather.main.temp);
            }

            return uniquedata;
        }
    }
}
