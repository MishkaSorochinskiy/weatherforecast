﻿using Application.IServices;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class SavedCityService : ISavedCityService
    {
        public IUnitOfWork _unitofwork { get; set; }

        public SavedCityService(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public async Task AddCity(string name)
        {
            bool isNotIn = !this._unitofwork.SavedPlaceRepository.GetAll()
                .ToList().Select(c => c.Name).Contains(name);

            if (isNotIn)
            {
                await this._unitofwork.SavedPlaceRepository.Create(new SavedPlace() { Name = name });

                await this._unitofwork.Commit();
            }
        }
    }
}
