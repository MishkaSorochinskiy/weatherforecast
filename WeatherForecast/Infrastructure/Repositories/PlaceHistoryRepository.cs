﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Domain.IRepositories;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class PlaceHistoryRepository : IPlaceHistoryRepository
    {
        private WeatherContext db;

        public PlaceHistoryRepository(WeatherContext context)
        {
            this.db = context;
        }

        public async Task Create(PlaceHistory item)
        {
           await db.PlacesHistory.AddAsync(item);
        }

        public async void Delete(int id)
        {
            PlaceHistory place = await db.PlacesHistory.FindAsync(id);
            if (place != null)
                db.PlacesHistory.Remove(place);
        }

        public async Task<PlaceHistory> Get(int id)
        {
            return await db.PlacesHistory.FindAsync(id);
        }

        public IEnumerable<PlaceHistory> GetAll()
        {
            return db.PlacesHistory.ToList();
        }

        public void Update(PlaceHistory item)
        {
            db.PlacesHistory.Update(item);
        }
    }
}
