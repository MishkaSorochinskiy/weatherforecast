﻿using Domain;
using Domain.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class SavedPlaceRepository : ISavedPlaceRepository
    {
        private WeatherContext db;

        public SavedPlaceRepository(WeatherContext context)
        {
            this.db = context;
        }

        public async Task Create(SavedPlace item)
        {
             await db.SavedPlaces.AddAsync(item);
        }

        public async void Delete(int id)
        {
            SavedPlace place = await db.SavedPlaces.FindAsync(id);
            if (place != null)
                db.SavedPlaces.Remove(place);
        }

        public async Task<SavedPlace> Get(int id)
        {
            return await db.SavedPlaces.FindAsync(id);
        }

        public IEnumerable<SavedPlace> GetAll()
        {
            return db.SavedPlaces.ToList();
        }

        public void Update(SavedPlace item)
        {
            db.SavedPlaces.Update(item);
        }
    }
}
