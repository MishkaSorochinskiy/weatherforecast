﻿using Domain;
using Domain.IRepositories;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        public WeatherContext db { get; private set; }
        private ISavedPlaceRepository savedPlaceRepository;
        private IPlaceHistoryRepository placeHistoryRepository;

        public IPlaceHistoryRepository PlaceHistoryRepository
        {
            get
            {
                if (placeHistoryRepository == null)
                {
                    placeHistoryRepository = new PlaceHistoryRepository(db);
                }

                return placeHistoryRepository;
            }
        }

        public ISavedPlaceRepository SavedPlaceRepository
        {
            get
            {
                if (savedPlaceRepository == null)
                {
                    savedPlaceRepository = new SavedPlaceRepository(db);
                }

                return savedPlaceRepository;
            }
        }

        public async Task Commit()
        {
             await db.SaveChangesAsync();
        }

        public UnitOfWork(WeatherContext database)
        {
            db = database;
        }
    }
}
