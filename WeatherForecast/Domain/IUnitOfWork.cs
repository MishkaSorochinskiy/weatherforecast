﻿using Domain.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IUnitOfWork
    {
        IPlaceHistoryRepository PlaceHistoryRepository { get; }
        ISavedPlaceRepository SavedPlaceRepository { get; }

        Task Commit();
    }
}
