﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.IRepositories
{
    public interface ISavedPlaceRepository:IRepository<SavedPlace>
    {
    }
}
