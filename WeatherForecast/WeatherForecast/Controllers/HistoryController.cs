﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WeatherForecast.Controllers
{
    public class HistoryController : Controller
    {
        public HistoryController()
        {

        }

        [HttpGet]
        public IActionResult GetHistory()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ClearHistory()
        {
            return RedirectToAction("GetHistory","History");
        }

    }
}