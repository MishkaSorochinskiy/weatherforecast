﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Application;
using Application.IServices;
using Domain;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WeatherForecast.Models;

namespace WeatherForecast.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _clientfactory;

        private readonly WeatherContext _context;

        private readonly IWeatherService _weather;
        private readonly ISavedCityService _cityservice;

        private readonly IUnitOfWork _unitofwork;


        public HomeController(ILogger<HomeController> logger, IHttpClientFactory clientfactory,
            WeatherContext context,IWeatherService weather,IUnitOfWork unitofwork,
            ISavedCityService cityservice)
        {
            _logger = logger;
            _clientfactory = clientfactory;
            _context = context;
            _weather = weather;
            _cityservice = cityservice;
            _unitofwork = unitofwork;
        }

        [HttpGet]
        public async  Task<IActionResult> Index(GetWeatherDto getweather)
        {
            ViewBag.days = getweather.Days;
            ViewBag.city = getweather.Location;
            ViewBag.savedcities = _unitofwork.SavedPlaceRepository.GetAll();

            if (ModelState.IsValid)
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                   $"{this._weather.WeatherLink}q={getweather.Location}&mode=json&appid={this._weather.ApiId}&units=metric");

                var client = _clientfactory.CreateClient();

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    
                    var data = await JsonSerializer.DeserializeAsync<WeatherData>(responseStream);

                    data=_weather.GetWeather(data, getweather.Days);

                    return View(data);
                }
            }

            return View();
        }

        public IActionResult History()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetCities()
        {
            return View(_unitofwork.SavedPlaceRepository.GetAll());
        }
        [HttpPost]
        public async Task<IActionResult> AddCity(AddCitiDto city)
        {
            if (ModelState.IsValid)
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                   $"{this._weather.WeatherLink}q={city.CityName}&mode=json&appid={this._weather.ApiId}&units=metric");

                var client = _clientfactory.CreateClient();

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();

                    var data = await JsonSerializer.DeserializeAsync<WeatherData>(responseStream);

                    await _cityservice.AddCity(data.city.name);

                }
            }

            return RedirectToAction("GetCities", "Home");
        }

    }
}
